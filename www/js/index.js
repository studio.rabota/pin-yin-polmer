/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {

        Sound.playSound("a", 1);

    }
};

var Sound = {};

Sound = {

    getMediaUrl : function(sound, tone) {

        var sound = "audio/" + sound + tone + ".mp3";

        // android
        if(window.cordova && device.platform.toLowerCase() === "android") return "/android_asset/www/" + sound;

        // browser
        return sound;

    },

    createSound : function(sound, tone) {

        var sound = this.getMediaUrl(sound, tone);

        // browser
        if(!window.cordova) return new Audio(sound);

        // android
        return new Media(

            sound,

            function (success) {
                // success
            },
            function (err) {
                // error
                console.log(JSON.stringify(err));
            }
        );

    },

    playSound : function(sound, tone) {

        var media = this.createSound(sound, tone);

        media.play();

    }

}

app.initialize();