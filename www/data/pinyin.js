/*
- JS object with PinYin data
- Requires lib/pinyin.js to be loaded for converting (wo2 to wó) : http://egukin.github.io/Pinyin.js/
*/
PinYin = {

  raw : {

    b: ['a', 'o', 'ai', 'ei', 'ao', 'an', 'en', 'ang', 'eng', 'i', 'iao',	'ie', 'ian', 'in', 'ing', 'u'	],
    p: ['a', 'o', 'ai', 'ei', 'ao', 'ou', 'an',	'en', 'ang', 'eng', 'i', 'iao', 'ie', 'ian', 'in', 'ing', 'u'],
    m: ['a', 'o', 'e', 'ai', 'ei', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'i', 'iao', 'ie', 'iu', 'ian', 'in', 'ing', 'u'],
    f: ['a', 'o', 'ei', 'ou', 'an', 'en', 'ang', 'eng'],

    d: ['a', 'e', 'ai', 'ei', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'ong', 'i', 'iao', 'ie', 'iu', 'ian', 'ing', 'u', 'uo', 'ui', 'uan', 'un'],
    t: ['a', 'e', 'ai', 'ei', 'ao', 'ou', 'an', 'ang', 'eng', 'ong', 'i', 'iao', 'ie', 'ian', 'ing', 'u', 'uo', 'ui', 'uan', 'un'],
    n: ['a', 'e', 'ai', 'ei', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'ong', 'i', 'ia', 'iao', 'ie', 'iu', 'ian', 'ing', 'u', 'uo', 'uan', 'ü', 'üe'],
    l: ['a', 'e', 'ai', 'ei', 'ao', 'ou', 'an', 'ang', 'eng', 'ong', 'i', 'ia', 'iao', 'ie', 'iu', 'ian', 'ing', 'u', 'uo', 'uan', 'ü', 'üe'],

    g: ['a', 'e', 'ai', 'ei', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'ong', 'u', 'ua', 'uo', 'uai', 'ui', 'uan', 'un', 'uang'],
    k: ['a', 'e', 'ai', 'ei', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'ong', 'u', 'ua', 'uo', 'uai', 'ui', 'uan', 'un', 'uang'],
    h: ['a', 'e', 'ai', 'ei', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'ong', 'u', 'ua', 'uo', 'uai', 'ui', 'uan', 'un', 'uang'],

    z: ['a', 'e', 'i', 'ai', 'ei', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'ong', 'u', 'uo', 'ui', 'uan', 'un'],
    c: ['a', 'e', 'i', 'ai', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'ong', 'u', 'uo', 'ui', 'uan', 'un'],
    s: ['a', 'e', 'i', 'ai', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'ong', 'u', 'uo', 'ui', 'uan', 'un'],

    zh: ['a', 'e', 'i', 'ai', 'ei', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'ong', 'u', 'ua', 'uo', 'uai', 'ui', 'uan', 'un', 'uang'],
    ch: ['a', 'e', 'i', 'ai', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'ong', 'u', 'ua', 'uo', 'uai', 'ui', 'uan', 'un', 'uang'],
    sh: ['a', 'e', 'i', 'ai', 'ei', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'u', 'ua', 'uo', 'uai', 'ui', 'uan', 'un', 'uang'],
    r: ['e', 'i', 'ao', 'ou', 'an', 'en', 'ang', 'eng', 'ong', 'u', 'ua', 'uo', 'ui', 'uan', 'un'],

    j: ['i', 'ia', 'iao', 'ie', 'iu', 'ian', 'in', 'iang', 'ing', 'iong', 'u', 'ue', 'uan', 'un'],
    q: ['i', 'ia', 'iao', 'ie', 'iu', 'ian', 'in', 'iang', 'ing', 'iong', 'u', 'ue', 'uan', 'un'],
    x: ['i', 'ia', 'iao', 'ie', 'iu', 'ian', 'in', 'iang', 'ing', 'iong', 'u', 'ue', 'uan', 'un'],

  },

  init: function() {

    for(initial in this.raw) {

      var PinYinItems = Array();

      this.initials.push({letter: initial});

      for (var c = 0; c < this.raw[initial].length; c++) {

        var final = this.raw[initial][c],
          combination = initial + final;

        // Item
        var PinYinItem = {
          initial: initial,
          final: final,
          combination: combination,
          tones: this.getTones(combination)
        };

        PinYinItems.push(PinYinItem);

      }

      this.combinations.push(PinYinItems);
    }

  },

  getTones: function(combination) {

    var tones = Array();

    for (var toneNr = 1; toneNr <= 4; toneNr++) {
      var tone = pinyinJs.convert(combination+toneNr);
      tones.push({tone: tone, nr: toneNr})
    }

    return tones;

  },

  combinations: [],

  initials: []

};

PinYin.init();
