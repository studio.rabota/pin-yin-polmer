module.exports = function (grunt) {

  // Load Grunt tasks declared in the package.json file
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  // Configure Grunt
  grunt.initConfig({

    // grunt-express - Server
    express: {
      all: {
        options: {
          port: 9000,
          hostname: "0.0.0.0",
          bases: [__dirname],
          livereload: false
        }
      }
    },

    // grunt-watch - Watch file changes
    watch: {
      all: {
        files: ['www/index.html', '**/*.scss'],
        tasks: ['sass'],
        options: {
          livereload: true
        }
      }
    },

    // grunt-open - Open browser
    open: {
      develop: {
        // Gets the port from the connect configuration
        path: 'http://localhost:<%= express.all.options.port%>/www/index.html'
      },
      build: {
        path: 'http://localhost:<%= express.all.options.port%>/www/build.html'
      }
    },

    // vulcanize - Compress Polymer
    vulcanize: {
      default: {
        options: {
          inlineScripts: true,
          inlineCss: true,
          excludes: ['polymer.html']
        },
        files: {
          'www/build.html': 'www/index.html'
        }
      }
    },

    // sass
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'www/sass',
          src: ['*.scss'],
          dest: 'www/stylesheets',
          ext: '.css'
        }]
      }
    },

    // cordova
    cordovacli: {
      options: {
        path: 'www',
        cli: 'cordova'  // cca or cordova
      },
      cordova: {
        options: {
          command: ['create', 'platform', 'plugin', 'build'],
          platforms: ['ios', 'android'],
          plugins: ['device', 'file', 'media'],
          path: 'www',
          id: 'com.studiorabota',
          name: 'PinYin'
        }
      },
      create: {
        options: {
          command: 'create',
          id: 'com.studiorabota',
          name: 'PinYin'
        }
      },
      add_platforms: {
        options: {
          command: 'platform',
          action: 'add',
          platforms: ['ios', 'android']
        }
      },
      add_plugins: {
        options: {
          command: 'plugin',
          action: 'add',
          plugins: [
            'battery-status',
            'camera',
            'console',
            'contacts',
            'device',
            'device-motion',
            'device-orientation',
            'dialogs',
            'file',
            'geolocation',
            'globalization',
            'inappbrowser',
            'media',
            'media-capture',
            'network-information',
            'splashscreen',
            'vibration'
          ]
        }
      },
      remove_plugin: {
        options: {
          command: 'plugin',
          action: 'rm',
          plugins: [
            'battery-status'
          ]
        }
      },
      build_ios: {
        options: {
          command: 'build',
          platforms: ['ios']
        }
      },
      build_android: {
        options: {
          command: 'build',
          platforms: ['android']
        }
      },
      emulate_android: {
        options: {
          command: 'emulate',
          platforms: ['android'],
          args: ['--target', 'Nexus5']
        }
      },
      run_android: {
        options: {
          command: 'run',
          platforms: ['android']
        }
      },
      add_facebook_plugin: {
        options: {
          command: 'plugin',
          action: 'add',
          plugins: [
            'com.phonegap.plugins.facebookconnect'
          ],
          args: ['--variable', 'APP_ID=fb12132424', '--variable', 'APP_NAME=myappname']
        }
      }
    }

  });

  // Serve and watch
  grunt.registerTask('default', [
    'express',
    'open:develop',
    'watch'
  ]);

  // Build web
  grunt.registerTask('build', [
    'vulcanize',
    'express',
    'open:build',
    'watch'
  ]);

  // Build app
  grunt.registerTask('app', [
    'vulcanize',
    'cordovacli:build_android'
  ]);

  // Run app on mobile
  grunt.registerTask('mobile', [
    'vulcanize',
    'cordovacli:run_android'
  ]);

};